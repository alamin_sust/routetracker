<%-- 
    Document   : register
    Created on : May 25, 2018, 4:07:49 PM
    Author     : md_al
--%>

<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="com.routetracker.connection.Database"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    Database db = new Database();
    db.connect();
    try {
        Statement st = db.connection.createStatement();
        String q = "";
        ResultSet rs;
        Statement st2 = db.connection.createStatement();
        String q2 = "";
        ResultSet rs2;
        
        String filterBy = request.getParameter("filterBy");
        String userId = request.getParameter("userId");
        
        if(filterBy.equals("none")) {
            q2="select * from route r";
        } else if( filterBy.equals("favourite")) {
            q2="select * from route r join favourite f on(r.id=f.route_id) where f.user_id="+userId;
        } else if( filterBy.equals("userSpecific")) {
            q2="select * from route r where user_id="+userId;
        }
        
        
        rs2 = st2.executeQuery(q2);
        
        String routsStr = "";
        while(rs2.next()) {
            
            q=" select avg(rating) as avgRating from rating where route_id="+rs2.getString("r.id");
            rs = st.executeQuery(q);
            rs.next();
            
            String avgRating="0";
            
            if(!rs.getString("avgRating").equals("null")&&!rs.getString("avgRating").equals("")) {
                avgRating=rs.getString("avgRating");
            }
            
            String route = "{\"userId\" : "+rs2.getString("r.user_id")
                    +", \"routeId\" : "+rs2.getString("r.id")
                    +", \"startingPoint\" : \""+rs2.getString("r.starting_point")
                    +"\", \"routeName\" : \""+rs2.getString("r.route_name")
                    +"\", \"areaType\" : \""+rs2.getString("r.area_type")
                    +"\", \"activityType\" : \""+rs2.getString("r.activity_type")
                    +"\", \"duration\" : \""+rs2.getString("r.duration")
                    +"\", \"fitness\" : \""+rs2.getString("r.fitness")
                    +"\", \"access\" : \""+rs2.getString("r.access")
                    +"\", \"warnings\" : \""+rs2.getString("r.warnings")
                    +"\", \"safetyNotes\" : \""+rs2.getString("r.safety_notes")
                    +"\", \"averageRating\" : "+avgRating+"}";
            if(!routsStr.equals("")) {
                routsStr+=", ";
            }
            routsStr+=route;
        }

        
    %>

 {
  "result" : 1,
   "routes" : [
 <%=routsStr%>
    ]
}

    
    <%    
    } catch (Exception e) {
    %>
    
    {
    "result" : 0
    }

    <%
        System.out.println(e);
    } finally {
        db.close();
    }
%>

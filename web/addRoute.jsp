<%-- 
    Document   : register
    Created on : May 25, 2018, 4:07:49 PM
    Author     : md_al
--%>

<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="com.routetracker.connection.Database"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    Database db = new Database();
    db.connect();
    try {
        Statement st = db.connection.createStatement();
        String q = "";
        ResultSet rs;
        Statement st2 = db.connection.createStatement();
        String q2 = "";
        ResultSet rs2;
        
        q2="select max(id+1) as mxid from route";
        rs2 = st2.executeQuery(q2);
        rs2.next();
        
        q="insert into route(id,user_id,starting_point,route_track,route_name,area_type,activity_type,duration,fitness,access,warnings,safety_notes)"
                + " values ("+rs2.getString("mxid")
                + ","+request.getParameter("userId")
                + ",'"+request.getParameter("startingPoint")+"'"
                + ",'"+request.getParameter("routeTrack")+"'"
                + ",'"+request.getParameter("routeName")+"'"
                + ",'"+request.getParameter("areaType")+"'"
                + ",'"+request.getParameter("activityType")+"'"
                + ",'"+request.getParameter("duration")+"'"
                + ",'"+request.getParameter("fitness")+"'"
                + ",'"+request.getParameter("access")+"'"
                + ",'"+request.getParameter("warnings")+"'"
                + ",'"+request.getParameter("safetyNotes")+"'"
                + ")";
        st.executeUpdate(q);
        
    %>

    {
    "result" : 1,
    "routeId" : <%=rs2.getString("mxid")%> 
    }
    
    <%    
    } catch (Exception e) {
    %>
    
    {
    "result" : 0
    }

    <%
        System.out.println(e);
    } finally {
        db.close();
    }
%>